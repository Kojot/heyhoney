package com.dev.koju.heyhoney.Communicator;

import android.util.Log;

import com.dev.koju.heyhoney.Api.RestClient;
import com.dev.koju.heyhoney.Api.RestService;
import com.dev.koju.heyhoney.LocalStorage.LocalStorage;
import com.dev.koju.heyhoney.Model.Product;
import com.dev.koju.heyhoney.Model.ProductList;

import java.util.Calendar;

import lombok.Getter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Koju on 08.12.2017.
 */

public class ProductListCommunicator extends BaseCommunicator{

    @Getter
    private ProductList productList;
    private String mDeviceId;
    private long weekAgo;
    private String sharedUserId;

    public ProductListCommunicator(String... params) {
        this.productList = new ProductList();
        this.mDeviceId = params[0];
        if(params.length > 1){
            this.sharedUserId = params[1];
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, -7);
        weekAgo = calendar.getTimeInMillis();
    }

    public void getProductListFromApi() {
        RestService api = RestClient.getRetrofit().create(RestService.class);

        Call<ProductList> call = api.getProductList(mDeviceId, weekAgo);
        call.enqueue(new Callback<ProductList>() {
            @Override
            public void onResponse(Call<ProductList> call, Response<ProductList> response) {
                ProductList listFromResponse = response.body();
                if(listFromResponse != null){
                    productList.setProductList(listFromResponse.getProductList());
                }
                onResponse.callback(productList.getProductList());
            }

            @Override
            public void onFailure(Call<ProductList> call, Throwable t) {
                Log.e("ERROR", t.getMessage());
                Log.e("ERROR", call.toString());
            }
        });
    }

    public void getSharedProductList() {
        RestService api = RestClient.getRetrofit().create(RestService.class);

        Call<ProductList> call = api.getSharedProductList(sharedUserId);
        call.enqueue(new Callback<ProductList>() {
            @Override
            public void onResponse(Call<ProductList> call, Response<ProductList> response) {
                ProductList listFromResponse = response.body();
                if(listFromResponse != null){
                    productList.setProductList(listFromResponse.getProductList());
                }
                onResponse.callback(productList.getProductList());
            }

            @Override
            public void onFailure(Call<ProductList> call, Throwable t) {
                Log.e("ERROR", t.getMessage());
                Log.e("ERROR", call.toString());
            }
        });
    }



    public void updateProductList(Product product){
        RestService api = RestClient.getRetrofit().create(RestService.class);

        Call<ProductList> call = api.updateProductList(product, weekAgo);
        call.enqueue(new Callback<ProductList>() {
            @Override
            public void onResponse(Call<ProductList> call, Response<ProductList> response) {
                ProductList listFromResponse = response.body();
                if(listFromResponse != null){
                    productList.setProductList(listFromResponse.getProductList());
                }
                onResponse.callback(productList.getProductList());
            }

            @Override
            public void onFailure(Call<ProductList> call, Throwable t) {
                Log.e("ERROR", t.getMessage());
                Log.e("ERROR", call.toString());
            }
        });
    }

    public void updateSharedProductList(Product product){
        RestService api = RestClient.getRetrofit().create(RestService.class);

        Call<ProductList> call = api.updateSharedProductList(product);
        call.enqueue(new Callback<ProductList>() {
            @Override
            public void onResponse(Call<ProductList> call, Response<ProductList> response) {
                ProductList listFromResponse = response.body();
                if(listFromResponse != null){
                    productList.setProductList(listFromResponse.getProductList());
                }
                onResponse.callback(productList.getProductList());
            }

            @Override
            public void onFailure(Call<ProductList> call, Throwable t) {
                Log.e("ERROR", t.getMessage());
                Log.e("ERROR", call.toString());
            }
        });
    }
}
