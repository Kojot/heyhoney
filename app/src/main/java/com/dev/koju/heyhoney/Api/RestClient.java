package com.dev.koju.heyhoney.Api;

import lombok.Getter;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Koju on 11.12.2017.
 */

public class RestClient {

//    private static final String URL = "http://10.0.2.2:8080/";
    private static final String URL = "https://quiet-cove-97741.herokuapp.com";

    static public Retrofit getRetrofit(){
        OkHttpClient client = new OkHttpClient();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
