package com.dev.koju.heyhoney.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.dev.koju.heyhoney.Api.RestClient;
import com.dev.koju.heyhoney.Api.RestService;
import com.dev.koju.heyhoney.Callback.CallbackResponse;
import com.dev.koju.heyhoney.Communicator.ApplicationInfoCommunicator;
import com.dev.koju.heyhoney.Communicator.BaseCommunicator;
import com.dev.koju.heyhoney.LocalStorage.LocalStorage;
import com.dev.koju.heyhoney.Model.ApplicationInfo;
import com.dev.koju.heyhoney.Model.JsonRepresentative;
import com.dev.koju.heyhoney.R;
import com.dev.koju.heyhoney.UserPerrmisions.PermissionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class MainActivity extends AppCompatActivity {

    private ApplicationInfo mApplicationInfo;
    private BaseCommunicator communicator;
    private String mDeviceId;
    private RestService client;

    @BindView(R.id.btn_start)
    Button mBtnStart;

    @OnClick(R.id.btn_start)
    void onClick(){
        mDeviceId = LocalStorage.getSharedPreference("deviceId", getApplicationContext());
        if(mDeviceId != null){
            Intent intent = new Intent(this, ProductListActivity.class);
            startActivity(intent);
        }
        else{
            Intent intent = new Intent(this, RegisterUserActivity.class);
            startActivityForResult(intent, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                setStartButtonText(R.string.btn_start);
                mApplicationInfo = (ApplicationInfo) data.getSerializableExtra("applicationInfo");
                if (mApplicationInfo != null) {
                    communicator = new ApplicationInfoCommunicator(mApplicationInfo.getDeviceInfo().getDeviceId());
                    communicator.sendRequest(mApplicationInfo, appInfo -> client.sendApplicationInfo((ApplicationInfo) appInfo));
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        client = RestClient.getRetrofit().create(RestService.class);

        mDeviceId = LocalStorage.getSharedPreference("deviceId", getApplicationContext());

        if(mDeviceId != null){
            setStartButtonText(R.string.btn_start);
        } else{
            setStartButtonText(R.string.btn_start_register);
        }
    }

    private void setStartButtonText(@StringRes int text) {
        mBtnStart.setText(text);
    }
}
