package com.dev.koju.heyhoney.View;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.EditText;

import com.dev.koju.heyhoney.Adapter.ProductListAdapter;
import com.dev.koju.heyhoney.Api.RestClient;
import com.dev.koju.heyhoney.Api.RestService;
import com.dev.koju.heyhoney.Callback.CallbackResponse;
import com.dev.koju.heyhoney.Communicator.BaseCommunicator;
import com.dev.koju.heyhoney.Communicator.ProductListCommunicator;
import com.dev.koju.heyhoney.LocalStorage.LocalStorage;
import com.dev.koju.heyhoney.Model.Product;
import com.dev.koju.heyhoney.Model.ProductList;
import com.dev.koju.heyhoney.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Koju on 09.02.2018.
 */

public class SharedProductListActivity extends Activity {
    private Product mProduct;
    private ProductListAdapter mAdapter;
    private ProductList mProductList;
    private BaseCommunicator communicator;
    private String mDeviceId;
    private RestService client;

    @BindView(R.id.shared_product_list)
    RecyclerView mRecyclerView;

    @BindView(R.id.input_shared_user_id)
    EditText mSharedUserId;

    @OnClick(R.id.btn_add_shared_product)
    void addProduct(){
        if(mSharedUserId.getText().toString().equals("")){
            mSharedUserId.setError("Provide user ID");
        }else{
            LocalStorage.setPreference("sharedUserId", mSharedUserId.getText().toString(), getApplicationContext());
            Intent intent = new Intent(this, CreateProductActivity.class);
            startActivityForResult(intent, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                mProduct = (Product) data.getSerializableExtra("createdProduct");
                if (mProduct != null) {
                    communicator = new ProductListCommunicator(mDeviceId);
                    communicator.setCallbackResponse(new CallbackResponse(){
                        @Override
                        public <T> void callback(T t) {
                            mProductList = new ProductList();
                            mProductList.setProductList((List<Product>) t);
                            mAdapter = new ProductListAdapter(mProductList);
                            mRecyclerView.setAdapter(mAdapter);
                        }
                    });
                    ((ProductListCommunicator) communicator).updateSharedProductList(mProduct);
                }
            }
        }
    }

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) { //TODO what with 0 value
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            Product productToRemove = mProductList.getProductList().get(position);
            communicator = new ProductListCommunicator(mDeviceId);
            communicator.sendRequest(productToRemove, product -> client.deleteProduct((Product) product));
            mProductList.getProductList().remove(position);
            mAdapter.setItems(mProductList);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_product_list);
        ButterKnife.bind(this);

        client = RestClient.getRetrofit().create(RestService.class);
        mDeviceId = LocalStorage.getSharedPreference("deviceId", getApplicationContext());

        ItemTouchHelper touchHelper = new ItemTouchHelper(simpleCallback);
        touchHelper.attachToRecyclerView(mRecyclerView);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }
}
