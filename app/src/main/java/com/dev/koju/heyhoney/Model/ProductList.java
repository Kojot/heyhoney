package com.dev.koju.heyhoney.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;


/**
 * Created by Koju on 11.12.2017.
 */

@Getter
@Setter
public class ProductList implements Serializable, JsonRepresentative{
    private List<Product> productList;

    public ProductList() {
        productList = new ArrayList<>();
    }

    public void addProduct(Product product) {
        productList.add(product);
    }
}
