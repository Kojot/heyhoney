package com.dev.koju.heyhoney.UserPerrmisions;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.dev.koju.heyhoney.LocalStorage.LocalStorage;

import lombok.Getter;

/**
 * Created by Koju on 10.01.2018.
 */

public class PermissionManager {
    private final int REQUEST_PERMISSION_PHONE_STATE = 1;
    private Context mContext;
    private Activity mActivity;
    @Getter
    private String mDeviceId;

    public PermissionManager(Context context, Activity activity) {
        mContext = context;
        mActivity = activity;
    }

    @SuppressLint("HardwareIds")
    public void showPhoneStatePermission() {
        int permissionCheck = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            int androidVersion = Build.VERSION.SDK_INT;
            if (androidVersion >= 23) {
                showExplanation("Unique device id", "Application need your permission to get unique device id for user management in online solutions like share product list option",
                        Manifest.permission.READ_PHONE_STATE, REQUEST_PERMISSION_PHONE_STATE);
            } else {
                requestPermission(Manifest.permission.READ_PHONE_STATE, REQUEST_PERMISSION_PHONE_STATE);
            }
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager != null) {
                mDeviceId = telephonyManager.getDeviceId();
                LocalStorage.setPreference("deviceId", mDeviceId, mContext);
            }else{
                Log.e("ERROR", "Cannot find device");
            }
        }
    }

    private void showExplanation(String title, String message, final String permission, final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission(permission, permissionRequestCode);
                    }
                });
        builder.create().show();
    }

    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(mActivity, new String[]{permissionName}, permissionRequestCode);
    }
}
