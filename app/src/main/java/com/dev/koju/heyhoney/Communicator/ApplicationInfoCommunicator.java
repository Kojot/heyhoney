package com.dev.koju.heyhoney.Communicator;

import android.util.Log;

import com.dev.koju.heyhoney.Api.RestClient;
import com.dev.koju.heyhoney.Api.RestService;
import com.dev.koju.heyhoney.Model.ApplicationInfo;
import com.dev.koju.heyhoney.Model.DeviceInfo;
import com.dev.koju.heyhoney.Model.User;

import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Koju on 16.01.2018.
 */

public class ApplicationInfoCommunicator extends BaseCommunicator {

    @Getter
    @Setter
    private ApplicationInfo mApplicationInfo;
    private String mDeviceId;

    public ApplicationInfoCommunicator(String deviceId) {
        mDeviceId = deviceId;
    }

    public void getApplicationInfo() {
        RestService api = RestClient.getRetrofit().create(RestService.class);

        Call<ApplicationInfo> call = api.getApplicationInfo(mDeviceId);
        call.enqueue(new Callback<ApplicationInfo>() {
            @Override
            public void onResponse(Call<ApplicationInfo> call, Response<ApplicationInfo> response) {
                ApplicationInfo infoFromResponse = response.body();
                if(infoFromResponse != null){
                    mApplicationInfo = new ApplicationInfo(
                            DeviceInfo.builder()
                                    .deviceId(infoFromResponse.getDeviceInfo().getDeviceId())
                                    .build(),
                            User.builder()
                                    .userName(infoFromResponse.getUser().getUserName())
                                    .userId(infoFromResponse.getUser().getUserId())
                                    .build());
                }
                onResponse.callback(mApplicationInfo);
            }

            @Override
            public void onFailure(Call<ApplicationInfo> call, Throwable t) {
                Log.e("ERROR", t.getMessage());
                Log.e("ERROR", call.toString());
            }
        });
    }
}
