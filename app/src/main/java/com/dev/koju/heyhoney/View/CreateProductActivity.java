package com.dev.koju.heyhoney.View;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.dev.koju.heyhoney.LocalStorage.LocalStorage;
import com.dev.koju.heyhoney.Model.Product;
import com.dev.koju.heyhoney.R;
import com.dev.koju.heyhoney.VoiceManagement.SpeechInterpreter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Koju on 24.12.2017.
 */

public class CreateProductActivity extends Activity {
    private static final int REQ_CODE_SPEECH_INPUT = 100;
    private SpeechInterpreter speechInterpreter;
    private ArrayAdapter<CharSequence> unitsInSpinner;
    private String mDeviceId;
    private String msharedUserId;


    @BindView(R.id.input_product_name)
    EditText mProductName;

    @BindView(R.id.input_product_quantity)
    EditText mProductQuantity;

    @BindView(R.id.spinner_product_unit)
    Spinner mUnitsSpinner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_product);
        ButterKnife.bind(this);
        msharedUserId = LocalStorage.getSharedPreference("sharedUserId", getApplicationContext());

        mDeviceId = LocalStorage.getSharedPreference("deviceId", getApplicationContext());
        unitsInSpinner = ArrayAdapter.createFromResource(this, R.array.spinner_product_unit, android.R.layout.simple_spinner_item);
        mUnitsSpinner.setAdapter(unitsInSpinner);
        unitsInSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    @OnClick(R.id.btn_voice_add)
    public void onVoiceAddProductClicked() {
        Log.d("DEBUG", "Voice button clicked");
        startVoiceInput();
    }

    private void startVoiceInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Jaki produkt chcesz dodać?");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException e) {
            Log.e("ERROR", e.getMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        speechInterpreter = new SpeechInterpreter();
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    speechInterpreter.interpret(result.get(0));
                    mProductName.setText(speechInterpreter.getMName());
                    mProductQuantity.setText(speechInterpreter.getMQuantity());
                    mUnitsSpinner.setSelection(unitsInSpinner.getPosition(speechInterpreter.getMUnit()));
                }
                break;
            }
        }
    }

    @OnClick(R.id.btn_confirm_new_product)
    public void confirmNewProductClicked() {
        Calendar calendar = Calendar.getInstance();
        Intent intent = new Intent();
        Product createdProduct = Product.builder()
                .name(mProductName.getText().toString())
                .quantity(mProductQuantity.getText().toString())
                .unit(mUnitsSpinner.getSelectedItem().toString())
                .deviceId(mDeviceId)
                .isBought(0)
                .timestamp(calendar.getTimeInMillis())
                .sharedUserId(msharedUserId)
                .build();
        if(validateProduct(createdProduct)) {
            intent.putExtra("createdProduct", createdProduct);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private boolean validateProduct(Product product){
        if(product.getName().equals("")){
            mProductName.setError("Name cannot be empty");
            return false;
        }
        if(product.getQuantity().equals("")){
            mProductQuantity.setError("Quantity cannot be empty");
            return false;
        }
        return true;
    }

    @OnClick(R.id.btn_cancel_new_product)
    public void cancelNewProduct() {
        finish();
    }
}
