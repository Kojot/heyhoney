package com.dev.koju.heyhoney.VoiceManagement;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;

/**
 * Created by Koju on 02.01.2018.
 */

@Getter
public class SpeechInterpreter {
    private String mName;
    private String mQuantity;
    private String mUnit;
    private int mIndex;

    public void interpret(String sample) {
        String[] separatedText = sample.split(" ");
        List<String> list = new LinkedList<>();
        list.addAll(Arrays.asList(separatedText));

        tryToFind(list);
    }

    private void tryToFind(List<String> list) {
        List<String> withoutQua = distinguishQuantity(list);
        withoutQua.remove(mIndex);
        List<String> withoutUnit = distinguishUnit(withoutQua);
        withoutUnit.remove(mIndex);
        mName = distinguishName(withoutUnit);
    }

    private String distinguishName(List<String> list){
        StringBuilder sb = new StringBuilder();
        for (String str: list) {
            sb.append(str).append(" ");
        }
        return sb.toString();
    }

    private List<String> distinguishUnit(List<String> list) {
        for(int i = 0; i < list.size(); i++){
            switch(list.get(i)){
                case "kilo" :
                case "kilogramów" :
                case "kg" :
                    mUnit = "kg";
                    mIndex = i;
                    break;
                case "litrów" :
                case "litra" :
                case "litr" :
                case "l" :
                    mUnit = "l";
                    mIndex = i;
                    break;
                case "sztuki" :
                case "sztuk" :
                case "sztuka" :
                    mUnit = "szt.";
                    mIndex = i;
                    break;
                case "metra" :
                case "metrów" :
                case "metr" :
                case "m" :
                    mUnit = "m";
                    mIndex = i;
                    break;
                default :
                    mUnit = "";
                    break;
            }
        }
        return list;
    }


    private List<String> distinguishQuantity(List<String> list) {
        int qua = 0;
        int index = 0;
        for (int i = 0; i < list.size(); i++) {
            try {
                qua = Integer.parseInt(list.get(i));
                if (qua != 0) {
                    mQuantity = Integer.toString(qua);
                    mIndex = i;
                    break;
                }
            } catch (NumberFormatException e) {
                e.getMessage();
            }
        }
        return list;
    }
}
