package com.dev.koju.heyhoney.Communicator;

import com.dev.koju.heyhoney.Model.JsonRepresentative;

import retrofit2.Call;

/**
 * Created by Koju on 05.02.2018.
 */
@FunctionalInterface
public interface Communicator {
    Call<String> setApiMethod (JsonRepresentative representative);
}

