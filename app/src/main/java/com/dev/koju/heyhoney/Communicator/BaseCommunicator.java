package com.dev.koju.heyhoney.Communicator;

import android.util.Log;

import com.dev.koju.heyhoney.Callback.CallbackResponse;
import com.dev.koju.heyhoney.Model.JsonRepresentative;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Koju on 11.01.2018.
 */

public abstract class BaseCommunicator {
    protected CallbackResponse onResponse;

    public void sendRequest(JsonRepresentative representative, Communicator func){
        Call<String> call = func.setApiMethod(representative);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.i("INFO", response.toString());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("ERROR", t.getMessage());
                Log.e("ERROR", call.toString());
            }
        });
    }

    public void setCallbackResponse(CallbackResponse callbackResponse){
        onResponse = callbackResponse;
    }
}
