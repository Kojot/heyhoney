package com.dev.koju.heyhoney.Adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.dev.koju.heyhoney.Model.Product;
import com.dev.koju.heyhoney.Model.ProductList;
import com.dev.koju.heyhoney.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Koju on 27.11.2017.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {

    private List<Product> mDataset = new ArrayList<>();

    public ProductListAdapter(final ProductList productList) {
        setItems(productList);
    }

    public void setItems(final ProductList productList){
        mDataset.clear();
        mDataset.addAll(productList.getProductList());
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindView(mDataset.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_product_name)
        TextView mProductName;

        @BindView(R.id.item_product_quantity)
        TextView mProductQuantity;

        @BindView(R.id.item_product_unit)
        TextView mProductUnit;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        void bindView(Product product){
            if(product.getIsBought() == 1){
                mProductName.setTextColor(Color.GREEN);
                mProductQuantity.setTextColor(Color.GREEN);
                mProductUnit.setTextColor(Color.GREEN);
                mProductName.setText(product.getName());
                mProductQuantity.setText(product.getQuantity());
                mProductUnit.setText(product.getUnit());
            } else{
                mProductName.setText(product.getName());
                mProductQuantity.setText(product.getQuantity());
                mProductUnit.setText(product.getUnit());
            }

        }
    }
}
