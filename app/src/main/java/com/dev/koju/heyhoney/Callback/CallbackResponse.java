package com.dev.koju.heyhoney.Callback;

/**
 * Created by Koju on 11.01.2018.
 */

public interface CallbackResponse {
   <T> void callback(T t);
}
