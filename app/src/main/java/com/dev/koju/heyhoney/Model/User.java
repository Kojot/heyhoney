package com.dev.koju.heyhoney.Model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koju on 10.01.2018.
 */

@Getter
@Builder
public class User implements Serializable{
    private String userName;
    private int userId;
}
