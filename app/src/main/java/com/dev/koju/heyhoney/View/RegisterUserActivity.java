package com.dev.koju.heyhoney.View;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.dev.koju.heyhoney.LocalStorage.LocalStorage;
import com.dev.koju.heyhoney.Model.ApplicationInfo;
import com.dev.koju.heyhoney.Model.DeviceInfo;
import com.dev.koju.heyhoney.Model.User;
import com.dev.koju.heyhoney.R;
import com.dev.koju.heyhoney.UserPerrmisions.PermissionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Koju on 16.01.2018.
 */

public class RegisterUserActivity extends Activity {
    private PermissionManager mPermissionManager;
    private ApplicationInfo mApplicationInfo;
    private String mDeviceInfo;

    @BindView(R.id.input_user_name)
    EditText mUserName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        ButterKnife.bind(this);

        mPermissionManager = new PermissionManager(this, this);
        mDeviceInfo = getDeviceId();
    }

    @OnClick(R.id.btn_confirm_new_user)
    public void onRegisterUserClicked(){
        if (validateUserName(mUserName.getText().toString())) {
            mDeviceInfo = getDeviceId();
            ApplicationInfo applicationInfo = new ApplicationInfo(
                    DeviceInfo.builder()
                            .deviceId(mDeviceInfo)
                            .build(),
                    User.builder()
                            .userName(mUserName.getText().toString())
                            .build()
            );
            LocalStorage.setPreference("userName", mUserName.getText().toString(), getApplicationContext());
            Intent intent = new Intent();
            intent.putExtra("applicationInfo", applicationInfo);
            setResult(RESULT_OK, intent);

            if(applicationInfo.getUser().getUserName() != null){
                finish();
            }
        }
    }

    private boolean validateUserName(String userName){
        if(userName.equals("")){
            mUserName.setError("User name cannot be empty");
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult( int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mDeviceInfo = getDeviceId();
                    Log.d("DEBUG", "DeviceId: " + mDeviceInfo);
                } else {
                    Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private String getDeviceId() {
        mPermissionManager.showPhoneStatePermission();
        return mPermissionManager.getMDeviceId();
    }
}
