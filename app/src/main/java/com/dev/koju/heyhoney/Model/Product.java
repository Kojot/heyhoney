package com.dev.koju.heyhoney.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koju on 02.12.2017.
 */

@Getter
@Builder
public class Product implements Serializable, JsonRepresentative{
    private int id;
    private String name;
    private String quantity;
    private String unit;
    private String deviceId;
    private int isBought;
    private long timestamp;
    private String sharedUserId;
}
