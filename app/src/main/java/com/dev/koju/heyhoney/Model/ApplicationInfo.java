package com.dev.koju.heyhoney.Model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Koju on 16.01.2018.
 */

@Getter
@Setter
public class ApplicationInfo implements Serializable, JsonRepresentative{
    private DeviceInfo deviceInfo;
    private User user;

    public ApplicationInfo(DeviceInfo deviceInfo, User user) {
        this.deviceInfo = deviceInfo;
        this.user = user;
    }
}
