package com.dev.koju.heyhoney.View;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.koju.heyhoney.Adapter.ProductListAdapter;
import com.dev.koju.heyhoney.Api.RestClient;
import com.dev.koju.heyhoney.Api.RestService;
import com.dev.koju.heyhoney.Callback.CallbackResponse;
import com.dev.koju.heyhoney.Communicator.ApplicationInfoCommunicator;
import com.dev.koju.heyhoney.Communicator.BaseCommunicator;
import com.dev.koju.heyhoney.Communicator.ProductListCommunicator;
import com.dev.koju.heyhoney.LocalStorage.LocalStorage;
import com.dev.koju.heyhoney.Model.ApplicationInfo;
import com.dev.koju.heyhoney.Model.JsonRepresentative;
import com.dev.koju.heyhoney.Model.Product;
import com.dev.koju.heyhoney.Model.ProductList;
import com.dev.koju.heyhoney.R;
import com.dev.koju.heyhoney.UserPerrmisions.PermissionManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * Created by Koju on 27.11.2017.
 */

public class ProductListActivity extends Activity {

    private Product mProduct;
    private ProductListAdapter mAdapter;
    private ProductList mProductList;
    private BaseCommunicator communicator;
    private String mDeviceId;
    private RestService client;

    @BindView(R.id.product_list)
    RecyclerView mRecyclerView;

    @BindView(R.id.user_name_from_db)
    TextView mUserName;

    @BindView(R.id.user_id)
    TextView mUserId;

    @OnClick(R.id.btn_add)
    void addBtnClick() {
        LocalStorage.setPreference("sharedUserId", "0", getApplicationContext());
        Intent intent = new Intent(this, CreateProductActivity.class);
        startActivityForResult(intent, 1);
    }

    @OnClick(R.id.btn_share)
    void shareBtnClicked() {
        Intent intent = new Intent(this, SharedProductListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_check_shared_list)
    void checkSharedProductList(){
        communicator = new ProductListCommunicator(mDeviceId, mUserId.getText().toString());
        communicator.setCallbackResponse(new CallbackResponse(){
            @Override
            public <T> void callback(T t) {
                mProductList = new ProductList();
                mProductList.setProductList((List<Product>) t);
                mAdapter = new ProductListAdapter(mProductList);
                mRecyclerView.setAdapter(mAdapter);
            }
        });
        ((ProductListCommunicator) communicator).getSharedProductList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                mProduct = (Product) data.getSerializableExtra("createdProduct");
                if (mProduct != null) {
                    communicator = new ProductListCommunicator(mDeviceId);
                    communicator.setCallbackResponse(new CallbackResponse(){
                        @Override
                        public <T> void callback(T t) {
                            mProductList = new ProductList();
                            mProductList.setProductList((List<Product>) t);
                            mAdapter = new ProductListAdapter(mProductList);
                            mRecyclerView.setAdapter(mAdapter);
                        }
                    });
                    ((ProductListCommunicator) communicator).updateProductList(mProduct);
                }
            }
        }
    }

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) { //TODO what with 0 value
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            Product productToRemove = mProductList.getProductList().get(position);
            communicator = new ProductListCommunicator(mDeviceId);
            if(productToRemove.getSharedUserId().equals("0")){
                communicator.sendRequest(productToRemove, product -> client.updateProduct((Product) product));
            }else{
                communicator.sendRequest(productToRemove, product-> client.deleteProduct((Product) product));
            }
            mProductList.getProductList().remove(position);
            mAdapter.setItems(mProductList);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);

        client = RestClient.getRetrofit().create(RestService.class);
        mDeviceId = LocalStorage.getSharedPreference("deviceId", getApplicationContext());

        communicator = new ProductListCommunicator(mDeviceId);
        communicator.setCallbackResponse(new CallbackResponse(){
            @Override
            public <T> void callback(T t) {
                mProductList = new ProductList();
                mProductList.setProductList((List<Product>) t);
                mAdapter = new ProductListAdapter(mProductList);
                mRecyclerView.setAdapter(mAdapter);
            }
        });
        ((ProductListCommunicator) communicator).getProductListFromApi();

        communicator = new ApplicationInfoCommunicator(mDeviceId);
        communicator.setCallbackResponse(new CallbackResponse() {
            @Override
            public <T> void callback(T t) {
                ApplicationInfo info = new ApplicationInfo(
                        ((ApplicationInfo) t).getDeviceInfo(),
                        ((ApplicationInfo) t).getUser());
                mUserName.setText(((ApplicationInfo) t).getUser().getUserName());
                mUserId.setText(String.valueOf(((ApplicationInfo) t).getUser().getUserId()));
            }
        });
        ((ApplicationInfoCommunicator) communicator).getApplicationInfo();

        ItemTouchHelper touchHelper = new ItemTouchHelper(simpleCallback);
        touchHelper.attachToRecyclerView(mRecyclerView);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }
}
