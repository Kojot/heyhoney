package com.dev.koju.heyhoney.Api;

import com.dev.koju.heyhoney.Model.ApplicationInfo;
import com.dev.koju.heyhoney.Model.Product;
import com.dev.koju.heyhoney.Model.ProductList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Koju on 11.12.2017.
 */

public interface RestService {

    @GET("/getProductList/{deviceId}/{time}")
    Call<ProductList> getProductList(@Path("deviceId") String deviceId, @Path("time") long time);

    @GET("/getSharedProductList/{sharedUserId}")
    Call<ProductList> getSharedProductList(@Path("sharedUserId") String sharedUserId);

    @POST("/updateProductList/{time}")
    Call<ProductList> updateProductList(@Body Product product, @Path("time") long time);

    @POST("/updateSharedProductList")
    Call<ProductList> updateSharedProductList(@Body Product product);

    @POST("/updateProduct")
    Call<String> updateProduct(@Body Product product);

    @POST("/deleteProduct")
    Call<String> deleteProduct(@Body Product product);

    @GET("/getApplicationInfo/{deviceId}")
    Call<ApplicationInfo> getApplicationInfo(@Path("deviceId") String deviceId);

    @POST("/sendApplicationInfo")
    Call<String> sendApplicationInfo(@Body ApplicationInfo info);
}
